#!/usr/bin/env ruby
# frozen_string_literal = true

# Dissonance Discord IRC bridge
# MediaMagnet 2019

require 'discordrb'
require 'socket'
require 'sugilite'
require 'oj'

config = Oj.load_file('config.json')
config = config[0]

server = config['server']
port = config['port']
nick = config['nickname']
# channel = config['channelMapping']['#tasbot-dev']
channel = '#magnet_test'

s = TCPSocket.open(server, port)
print('addr: ', s.addr.join(':'), "\n")
print('peer: ', s.peeraddr.join(':'), "\n")
s.puts 'USER mediamag 0 * mediamag'
s.puts "NICK #{nick}"
s.puts "JOIN #{channel}"
s.puts "PRIVMSG #{channel} :Dissonance IRC Bridge cause discord-irc be dumb."

until s.eof?
  bot = Discordrb::Bot.new token: config['discordToken']
  msg = s.gets
  msg = SugiliteIRC::Message.new(msg)
  wholemsg = "<#{msg.sender}>: #{msg.message}"
  if msg.sender == "MediaMagnet"
    bot.send_message(config['channelMapping'].keys[0], wholemsg)
    puts wholemsg
  end
end

