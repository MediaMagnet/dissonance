#!/usr/bin/env ruby
# frozen_string_literal = true

require 'oj'
require 'discordrb'
require 'drb/drb'

config = Oj.load_file('config.json')
config = config[0]

bot = Discordrb::Bot.new token: config['discordToken']

bot.message do |event|
  if event.channel.id.to_s == config['channelMapping'].keys[0]
    wholedismsg = "Discord: <#{event.author.name}>: #{event.content}"
    puts wholedismsg
  end
end
bot.run


